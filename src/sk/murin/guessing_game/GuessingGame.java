package sk.murin.guessing_game;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Random;

public class GuessingGame {
    private final JFrame frame;
    private final JTextField field;
    private int computerNumber;
    private int attempts;
    private final JLabel labelAttempts;
    private final JLabel labelTextInfo;

    private final ImageIcon imgs2;

    private int aa;
    private int bb;
    static Random r = new Random();

    public GuessingGame() throws IOException {
        frame = new JFrame("Hádaj číšlo");
        frame.setSize(600, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(null);
        Font f = new Font("Courier", Font.BOLD, 18);
        field = new JTextField();
        field.setBounds(400, 175, 70, 30);
        field.setFont(f);
        field.setBorder(new LineBorder(Color.black, 2));

        ImageIcon img = new ImageIcon("question.png");
        JLabel labelPicture = new JLabel();
        labelPicture.setIcon(img);
        labelPicture.setBounds(70, 100, 370, 250);
        imgs2 = new ImageIcon("flag.png");
        frame.setIconImage(ImageIO.read(new File("money.png")));

        JButton btnChangeRange = new JButton("Zmeň rozpätie");
        btnChangeRange.setFont(f);
        btnChangeRange.setFocusable(false);
        btnChangeRange.setBounds(400, 80, 180, 50);

        btnChangeRange.addActionListener(e -> showDialogs());

        JLabel labelMainMsg = new JLabel("Uhádni číslo a vyhraj jackpot");
        labelMainMsg.setFont(new Font("Courier", Font.BOLD, 28));
        labelMainMsg.setBounds(20, 15, 600, 50);
        labelMainMsg.setForeground(Color.RED);

        labelTextInfo = new JLabel("Sem napíš číslo od " + aa + " do " + bb);
        labelTextInfo.setBounds(395, 145, 190, 30);
        JButton btnSubmit = new JButton("Porovnaj");
        btnSubmit.setBounds(480, 175, 100, 30);
        btnSubmit.setFont(new Font("Courier", Font.BOLD, 14));
        btnSubmit.addActionListener(e -> compareNumbers());
        JButton btnEnd = new JButton("Vzdávam sa");
        btnEnd.setFont(f);
        btnEnd.setBounds(400, 280, 180, 70);
        btnEnd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] options = {"Naozaj", " Ok, rozmyslel som si to"};
                int vysledok = JOptionPane.showOptionDialog(null, "Naozaj sa vzdávaš? ", "Okamih pravdy ",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.INFORMATION_MESSAGE, imgs2, options, 0);
                if (vysledok == 1) {
                    field.setText("");
                    labelAttempts.setText("Počet pokusov " + 0);
                } else {
                    JOptionPane.showMessageDialog(null, "Dovi díky za hru  "
                            , "Zbohom",
                            JOptionPane.INFORMATION_MESSAGE);
                    frame.dispose();
                }
            }
        });

        labelAttempts = new JLabel("Počet pokusov " + attempts);

        labelAttempts.setBounds(80, 60, 180, 50);
        labelAttempts.setFont(f);

        frame.add(btnSubmit);
        frame.add(btnEnd);
        frame.add(labelTextInfo);
        frame.add(btnChangeRange);

        frame.add(labelAttempts);
        frame.add(labelPicture);
        frame.add(labelMainMsg);
        frame.add(field);
        frame.setResizable(false);
        frame.setVisible(true);
        showDialogs();
    }


    public void compareNumbers() {
        String userInput = field.getText();
        int userInputIntNumber;
        try {
            userInputIntNumber = Integer.parseInt(userInput);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Zadaj správny vstup", "Zadaj čísla", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        field.setText("");
        attempts++;
        labelAttempts.setText("Počet pokusov " + attempts);
        if (userInputIntNumber > computerNumber) {
            JOptionPane.showMessageDialog(null, "Tvoje číslo je väčšie", "Skús menšie", JOptionPane.INFORMATION_MESSAGE);
        } else if (userInputIntNumber < computerNumber) {
            JOptionPane.showMessageDialog(null, "Tvoje číslo je menšie", "Skús väčšie", JOptionPane.INFORMATION_MESSAGE);
        } else {
            String[] moznosti = {"Hrať ďalej", " Už nie"};

            int result = JOptionPane.showOptionDialog(null, "Uhádol si,potreboval si na to " + attempts +
                            "  pokusov, hrať ďalej?", "Chceš hrať ďalej?", JOptionPane.YES_NO_OPTION,
                    JOptionPane.INFORMATION_MESSAGE, null, moznosti, 0);

            computerNumber = r.nextInt(bb - aa) + aa;
            System.out.println(computerNumber);
            if (result == 0) {
                labelAttempts.setText("Počet pokusov : 0");
            } else {
                JOptionPane.showMessageDialog(null, "Dovi díky za hru,na " +
                                "uhádnutie čísla si potreboval " + attempts + " pokusov", "Zbohom",
                        JOptionPane.INFORMATION_MESSAGE);
                frame.dispose();
            }
            attempts = 0;
        }
    }

    public void showDialogs() {
        while (true) {
            try {
                String a = JOptionPane.showInputDialog(null, "Od akého čísla chceš hrať", "Od akého čísla chceš hrať", JOptionPane.QUESTION_MESSAGE);
                aa = Integer.parseInt(a);
                String b = JOptionPane.showInputDialog(null, "Do akého čísla chceš hrať", "Do akého čísla chceš hrať", JOptionPane.QUESTION_MESSAGE);
                bb = Integer.parseInt(b);
                if (aa < bb) {
                    computerNumber = r.nextInt(bb - aa) + aa;
                    System.out.println(computerNumber);
                    labelTextInfo.setText("Sem napíš číslo od " + aa + " do " + bb);
                    break;
                } else {
                    JOptionPane.showMessageDialog(null, "Zadaj čísla v správnom rozsahu", "Prvé musí byť menšie ako druhé", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Zadaj čísla", "Len čísla !!", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

}
